function hacerPeticion() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums";
    const txtId = document.getElementById("txtId").value;
    const res = document.getElementById("lista");
    const body = document.body;

    // Limpiar el contenido previo de la tabla y quitar la clase de fondo
    res.innerHTML = "<tbody>";
    body.classList.remove("fondo-no-encontrado");

    // Validar la respuesta
    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            // Aquí se dibuja la página
            const json = JSON.parse(this.responseText);

            // Variable para verificar si se encontró la ID
            let idEncontrada = false;

            // Ciclo para ir tomando cada uno de los registros
            for (const datos of json) {
                // Verificar si la ID coincide con la buscada
                if (datos.id == txtId) {
                    res.innerHTML += '<tr><td colspan="3">' + datos.title + '</td></tr>';
                    idEncontrada = true;
                    break; // Termina el bucle una vez que se encuentra la ID
                }
            }

            // Si no se encontró la ID, agregar la clase de fondo
            if (!idEncontrada) {
                res.innerHTML += '<tr><td colspan="3">ID no encontrada</td></tr>';
                body.classList.add("fondo-no-encontrado");
            }

            res.innerHTML += "</tbody>";
        }
    };

    http.open('GET', url, true);
    http.send();
}

// Codificar el botón de búsqueda
document.getElementById("btnBuscar").addEventListener("click", function () {
    hacerPeticion();
});

