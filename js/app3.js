document.addEventListener("DOMContentLoaded", function () {
    const btnCargar = document.getElementById("btnCargar");
    const btnBuscar = document.getElementById("btnBuscar");
    const txtId = document.getElementById("txtId");
    const listaUsuarios = document.getElementById("listaUsuarios");

    btnCargar.addEventListener("click", function () {
        hacerPeticion("https://jsonplaceholder.typicode.com/users");
    });

    btnBuscar.addEventListener("click", function () {
        const userId = txtId.value.trim();
        if (userId !== "") {
            hacerPeticion(`https://jsonplaceholder.typicode.com/users/${userId}`);
        } else {
            alert("Por favor, ingrese una ID para buscar.");
        }
    });

    function hacerPeticion(url) {
        const http = new XMLHttpRequest();
        listaUsuarios.innerHTML = "<tbody>";

        http.onreadystatechange = function () {
            if (this.status == 200 && this.readyState == 4) {
                const json = JSON.parse(this.responseText);

                if (Array.isArray(json)) {
                    if (json.length > 0) {
                        for (const usuario of json) {
                            listaUsuarios.innerHTML += `<tr>
                                <td>${usuario.id}</td>
                                <td>${usuario.name}</td>
                                <td>${usuario.username}</td>
                                <td>${usuario.email}</td>
                                <td>${usuario.address.street}</td>
                                <td>${usuario.address.suite}</td>
                                <td>${usuario.address.city}</td>
                                <td>${usuario.address.zipcode}</td>
                                <td>${usuario.address.geo.lat}</td>
                                <td>${usuario.address.geo.lng}</td>
                                <td>${usuario.phone}</td>
                                <td>${usuario.website}</td>
                                <td>${usuario.company.name}</td>
                                <td>${usuario.company.catchPhrase}</td>
                                <td>${usuario.company.bs}</td>
                            </tr>`;
                        }
                    } else {
                        listaUsuarios.innerHTML += '<tr><td colspan="3">No hay usuarios disponibles</td></tr>';
                    }
                } else {
                    // Si no es un array, se trata de la respuesta de búsqueda por ID
                    listaUsuarios.innerHTML += `<tr>
                        <td>${json.id}</td>
                        <td>${json.name}</td>
                        <td>${json.username}</td>
                        <td>${json.email}</td>
                        <td>${json.address.street}</td>
                        <td>${json.address.suite}</td>
                        <td>${json.address.city}</td>
                        <td>${json.address.zipcode}</td>
                        <td>${json.address.geo.lat}</td>
                        <td>${json.address.geo.lng}</td>
                        <td>${json.phone}</td>
                        <td>${json.website}</td>
                        <td>${json.company.name}</td>
                        <td>${json.company.catchPhrase}</td>
                        <td>${json.company.bs}</td>
                    </tr>`;
                }

                listaUsuarios.innerHTML += "</tbody>";
            }
        };

        http.open('GET', url, true);
        http.send();
    }
});