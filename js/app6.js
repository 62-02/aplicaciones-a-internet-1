document.addEventListener("DOMContentLoaded", function () {
    const btnBuscar = document.getElementById("btnBuscar");
    const inputPais = document.getElementById("inputPais");
    const resultado = document.getElementById("resultado");
    const capitalElement = document.getElementById("capital");
    const lenguajeElement = document.getElementById("lenguaje");

    btnBuscar.addEventListener("click", function () {
        const nombrePais = inputPais.value.trim();
        if (nombrePais !== "") {
            fetch(`https://restcountries.com/v3.1/name/${nombrePais}`)
                .then(response => response.json())
                .then(data => {
                    if (data.length > 0) {
                        const pais = data[0];
                        capitalElement.textContent = `Capital: ${pais.capital}`;
                        lenguajeElement.textContent = `Lenguaje: ${Object.values(pais.languages)[0]}`;
                        
                        resultado.style.display = "block";
                    } else {
                        resultado.style.display = "none";
                        alert("No se encontraron resultados para el país ingresado.");
                    }
                })
                .catch(error => {
                    console.error("Error en la petición:", error);
                    alert("Error al realizar la búsqueda.");
                });
        } else {
            alert("Por favor, ingrese el nombre de un país para buscar.");
        }
    });
});
